import { ApiProperty } from "@nestjs/swagger";

export class QuotesDto {
    @ApiProperty()
    readonly title: string;

    @ApiProperty()
    readonly author: string;
}
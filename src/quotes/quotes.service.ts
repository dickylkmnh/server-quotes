import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { IQuote } from './interface/quotes-interface';
import { Document } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class QuotesService {
    constructor(@InjectModel('Quote') private readonly quoteModel: Model<IQuote & Document>) { }
    quotes: IQuote[] = [
        {
            id: '1',
            title: 'official iridescent',
            author: 'muhammad ihsan'
        },
        {
            id: '2',
            title: 'iloomilo',
            author: 'dicky lukman hakim'
        },
        {
            id: '3',
            title: 'defra',
            author: 'defry galuh pidia'
        }
    ]

    // get all data
    getAllQuotes(): Promise<IQuote[]> {
        return this.quoteModel.find().exec();
    }

    // get data by id
    getQuotesById(id: string): IQuote {
        return this.quotes.find(quote => quote.id === id);
    }

    // create new data
    createQuotes(quote: IQuote) {
        const newQuote = new this.quoteModel(quote);
        console.log(newQuote);
        return newQuote;
    }

    // update data
    updateQuotes(id: string, updateQuoteDto): IQuote {
        const data = this.quotes.find(quote => quote.id === id);
        data.title = updateQuoteDto.title ? updateQuoteDto.title : data.title;
        data.author = updateQuoteDto.author ? updateQuoteDto.author : data.author;
        return data;
    }

    // delete data
    deleteQuotes(id: string) {
        return this.quotes.find(quote => quote.id === id);
    }
}

import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { QuotesController } from './quotes.controller';
import { QuotesService } from './quotes.service';
import { QuotesSchema } from './schema/quotes-schema';

@Module({
    imports: [MongooseModule.forFeature([{
        name: 'Quote',
        schema: QuotesSchema
    }])],
    controllers: [QuotesController],
    providers: [QuotesService]
})
export class QuotesModule { }

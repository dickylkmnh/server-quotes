import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { ApiParam, ApiTags } from '@nestjs/swagger';
import { identity } from 'rxjs';
import { QuotesDto } from './dto/quotes.dto';
import { IQuote } from './interface/quotes-interface';
import { QuotesService } from './quotes.service';

@ApiTags('quotes')
@Controller('quotes')
export class QuotesController {
    constructor(private quotesService: QuotesService) { }

    // get all data
    @Get()
    getAllQuotes(): Promise<IQuote[]> {
        return this.quotesService.getAllQuotes();
    }

    // get data by id
    @ApiParam({ name: 'id' })
    @Get(':id')
    getQuotesById(@Param('id') id): IQuote {
        return this.quotesService.getQuotesById(id);
    }

    // create new data
    @Post()
    createQuotes(@Body() quotesDto: QuotesDto): IQuote {
        return this.quotesService.createQuotes(quotesDto);
    }

    // update data
    @ApiParam({ name: 'id' })
    @Put(':id')
    updateQuotes(@Param('id') id, @Body() updateQuoteDto: QuotesDto): IQuote {
        return this.quotesService.updateQuotes(id, updateQuoteDto);
    }

    // delete data
    @ApiParam({ name: 'id' })
    @Delete(':id')
    deleteQuotes(@Param('id') id): IQuote {
        return this.quotesService.deleteQuotes(id);
    }
}
// git

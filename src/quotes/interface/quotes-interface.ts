export interface IQuote {
    id?: string,
    title: string,
    author: string
}
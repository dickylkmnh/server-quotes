import * as mongoose from 'mongoose';

export const QuotesSchema = new mongoose.Schema({
    title: String,
    author: String
});